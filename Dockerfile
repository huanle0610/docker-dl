FROM busybox
ARG NEO4J_VERSION="neo4j-desktop-offline-1.2.7-x86_64.AppImage"

WORKDIR /var/www

#RUN wget -c "https://neo4j.com/artifact.php?name=$NEO4J_VERSION" -O $NEO4J_VERSION
RUN wget -c https://huggingface.co/ggerganov/whisper.cpp/resolve/90a64d80ea254cf67575b41a5971f972c79f7b45/ggml-large-v1.bin?download=true
RUN wget -c https://huggingface.co/ggerganov/whisper.cpp/resolve/90a64d80ea254cf67575b41a5971f972c79f7b45/ggml-medium.en.bin?download=true

